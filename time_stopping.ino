#include <LiquidCrystal.h>
#include <Wire.h>
#include "TimerOne.h"  

#define COUNT_DOWN_VALUE 5

enum _STATES_ {
  SYNCH,
  STARTED,
  WINNER,
  TARGET_NOT_READY,
  DONE,
  RESETED,
  TARGET_RESETED,
  TARGET_NOT_RESETED
};

struct _states_ {
  bool synched;
  bool started;
  bool ready_state;
  bool winner;
  bool other_target_ready;
  bool own_target_ready;
  bool other_user_done;
  bool other_user_reseted;
} typedef state_struct;

state_struct states;

long last_sent_synch_message = 0;
long last_received_synch_message = 0;
long last_sent_target_message = 0;

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

const int START_BUTTON = A0;
const int STOP_BUTTON = A1;

const int GREEN_LIGHT = 8;
const int RED_LIGHT = 9;
const int WINNER_LAMP = 10;


int start_button_state = 0;
int stop_button_state = 0;

unsigned long time_ref = 0;
unsigned long time_current = 0;
unsigned long milli_seconds = 0;

unsigned long minutes = 0;
unsigned long seconds = 0;
unsigned long centi_seconds = 0;

//-------------------------------------------------------------------------------
// setup() set up all the initial values and pins
//
void setup() {

  Serial.begin(9600);
  
  lcd.begin(16,2);
  displayPrompt();
  displayPressStart();
  
  pinMode(START_BUTTON, INPUT);
  analogWrite(START_BUTTON, LOW);
  pinMode(STOP_BUTTON, INPUT);
  analogWrite(STOP_BUTTON, LOW);
  pinMode(RED_LIGHT, OUTPUT);
  pinMode(GREEN_LIGHT, OUTPUT);
  pinMode(WINNER_LAMP, OUTPUT);
  digitalWrite(RED_LIGHT, HIGH);
  digitalWrite(GREEN_LIGHT, LOW);
  digitalWrite(WINNER_LAMP, LOW);
  
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveMessage); 
  
  Timer1.initialize(1000000);
  
  resetStates();

}

//-------------------------------------------------------------------------------
// receiveMessage() interrupt function for received messages
//
void receiveMessage(int bytes_to_receive)
{
  while(0 < Wire.available()) // loop through all but the last
  {
    char message = Wire.read(); // receive byte as a character
    switch(message) {
      case SYNCH:
        states.synched = true;
        last_received_synch_message = millis();
        Serial.println("I received synch message");
        break;
      case STARTED:
        states.started = true;
        Serial.println("I received a STARTED message.");
        break;  
      case WINNER:
        Serial.println("I received a WINNER message.");
        states.winner = false;
        break;
      case DONE:
        Serial.println("I received a DONE message.");
        states.other_user_done = true;
        break;
      case RESETED:
        Serial.println("I received a RESETED message.");
        states.other_user_reseted = true;
        break;
      case TARGET_RESETED:
        Serial.println("I received a TARGET_RESETED message.");
        states.other_target_ready = true;
        break;
      case TARGET_NOT_RESETED:
        Serial.println("I received a TARGET_NOT_RESETED message.");
        states.other_target_ready = false;
        break;
      default:
        Serial.println("I received an UNKOWN message.");
    }

  }

}

//-------------------------------------------------------------------------------
// blinkWinner() function for the timer interrupt
//
void blinkWinner() {
   digitalWrite(WINNER_LAMP, digitalRead(WINNER_LAMP) ^ 1);
}

//-------------------------------------------------------------------------------
// displayPressStart() displays the the start prompt in the second row
//
void displayPressStart() {
  lcd.setCursor(0, 1);
  lcd.print("START druecken!");
}

//-------------------------------------------------------------------------------
// displayResetStopButton() "Ziele aufstellen" in the first row
//
void displayResetStopButton() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ziele aufstellen");
}

//-------------------------------------------------------------------------------
// displayResetStopButton() "Ziele aufstellen" in the first row
//
void displayResetStopButtonSecondRow() {
  lcd.setCursor(0, 1);
  lcd.print("Ziele aufstellen");
}

//---------------------------------------------------------------------------------
// displayPrompt() displays the prompt "www.pyrowerk.at"  in the first row
//
void displayPrompt() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("www.pyrowerk.at");
}

//---------------------------------------------------------------------------------
// displayWinnerMessage() displays the "Gewinner" in the first row
//
void displayWinnerMessage() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Gewinner!");
}

//-------------------------------------------------------------------------------
// displayResetStopButton() "Verlierer" in the first row
//
void displayLoserMessage() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Verlierer!");
}

//-------------------------------------------------------------------------------
// displayResetStopButton() "Stand gesperrt" in the first row
//
void otherUserNotDone() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Stand gesperrt");
}

//---------------------------------------------------------------------------------
// displayResult() calls the display Function corresponding to the result
//
void displayResult() {
  if(states.winner && states.synched) {
    displayWinnerMessage();
  } else if(!states.winner && states.synched) {
    displayLoserMessage();
  } else {
    displayPrompt();
  }
}

//---------------------------------------------------------------------------------
// displayTime() displays the calculated time
//
void displayTime() {
  lcd.setCursor(4,1);
  if(minutes < 10) {
    lcd.print("0");
  }
  lcd.print(minutes);
  lcd.print(":");
  
  if(seconds < 10) {
    lcd.print("0");
  }
  lcd.print(seconds);
  lcd.print(".");
  
  if(centi_seconds < 10) {
    lcd.print("0");
  }
  lcd.print(centi_seconds);
}

//---------------------------------------------------------------------------------
// calculateTime() calculates the centic_seconds, seconds and minutes
//
void calculateTime() {
  time_current = millis();
  milli_seconds = time_current - time_ref;
    
  centi_seconds = (milli_seconds % 1000)/ 10;
  seconds = (milli_seconds / 1000) % 60 ;
  minutes = (milli_seconds / 60000) % 60;
}

//---------------------------------------------------------------------------------
// switchLights() turns one light on and switches the other off
// l1 is turned on
// l2 is switched off
//
void switchLights(int l1, int l2) {
  digitalWrite(l1, HIGH);
  digitalWrite(l2, LOW);
}


//---------------------------------------------------------------------------------
// startCountDown() makes a countdown from
// start_value is the time, this function starts
//
void startCountDown(int start_value) {
  for(int i = start_value; i > 0; i--) {
    displayPrompt();
    lcd.setCursor(6, 1);
    lcd.print(i);
    delay(250);
    lcd.print(".");
    delay(250);
    lcd.print(".");
    delay(250);
    lcd.print(".");
    delay(250);
  }
}

//---------------------------------------------------------------------------------
// startTimer() measures the time
//
void startTimer() {
  time_ref = millis();
  while(1) {
    stop_button_state = analogRead(STOP_BUTTON);
    //BUTTON is HIGH
    if(stop_button_state > 100) { //button is HIGH

      if(states.synched) {
      
        if(states.winner) {
          sendMessage(WINNER, 4);
          Serial.println("I sent a WINNER message.");
          
          sendMessage(DONE, 4);
          Serial.println("I sent a DONE message.");

          return;
        } else {
          Serial.println("I sent a LOSER message.");
          sendMessage(DONE, 4);
          Serial.println("I sent a DONE message.");
          
          return;
        }
        
      } else {
        Serial.println("I am NOT synched");
        return;
      }
      
    }
    
    displayPrompt();
    calculateTime();
    displayTime();

    //delay -> no flicking on display...
    delay(30);
  }
}

//---------------------------------------------------------------------------------
// resetFunction() waits until the users presses START again.
//
void resetFunction() {
  displayResult();
  displayTime();
  
  while(1) {
    stop_button_state = analogRead(STOP_BUTTON);
    start_button_state = analogRead(START_BUTTON);
        
    if(states.synched) {
      
      sendTargetMessage();

      if(states.winner) {
        Timer1.attachInterrupt(blinkWinner);
      }
      
      if(states.other_user_reseted) {
        return;
      }
      
      if(!states.other_user_done) { //other user is still running
        Serial.println("Other User is not done...");
        if(start_button_state > 100) {
          otherUserNotDone();
          displayTime();
          delay(30);
        }
      } else if(states.own_target_ready && states.other_target_ready && states.other_user_done) { //other user is DONE...
        if(start_button_state > 100) {
          //send reset message
          sendMessage(RESETED, 4);
          Serial.println("I sent RESETED message");
          return;
        }
      } else if(stop_button_state > 100 || !states.other_target_ready) { //one target is NOT reseted
        if(start_button_state > 100) {
          displayResetStopButton();
          displayTime();
          Serial.println("Targets are NOT reseted");
          delay(30);
        }
      }
      
    } else {
      
      if(stop_button_state > 100) { //one target is NOT reseted
        if(start_button_state > 100) {
          displayResetStopButton();
          displayTime();
          Serial.println("Targets are NOT reseted");
          delay(30);
        }
      } else {
        if(start_button_state > 100) {
          return;
        }
      }
    }
  } 
}

//---------------------------------------------------------------------------------
// sendTargetMessage() sends a message corresponding to the status
// of the target state
//
void sendTargetMessage() {
  long time = millis();
  if((time - last_sent_target_message) > 150) { //send message every 150 milliseconds
    last_sent_target_message = time;
    
    if(analogRead(STOP_BUTTON) < 100) {
      sendMessage(TARGET_RESETED, 4);
      Serial.println("I am sending TARGET_RESETED message...");
      states.own_target_ready = true;
    } else {
      sendMessage(TARGET_NOT_RESETED, 4);
      Serial.println("I am sending TARGET_RESETED message...");
      states.own_target_ready = false;
    }
  }
  
}

//---------------------------------------------------------------------------------
// tryToSendSynchMessage() sends a synchronisation message
// resets the states if the last received message is 1000 milli seconds
// in the past
//
void tryToSendSynchMessage() {
  long time = millis();

  if((time - last_received_synch_message) > 1000 && states.synched) {
    resetStates();
  }
  
  if((time - last_sent_synch_message) > 250) { //send message every 250 milliseconds
    last_sent_synch_message = time;
    sendMessage(SYNCH, 4);
    Serial.println("I am sending SYNCH message...");
  }
}

//---------------------------------------------------------------------------------
// sendMessage() sends a message over the bus
// message is send
// bus which is used to send the message
//
void sendMessage(int message, int bus) {
  Wire.beginTransmission(bus);
  Wire.write(message);
  Wire.endTransmission();
}

//---------------------------------------------------------------------------------
// resetStates() resets all the states
//
void resetStates() {
  states.synched = false;
  states.started = false;
  states.ready_state = true;
  states.winner = true;
  states.other_target_ready = false;
  states.own_target_ready = true;
  states.other_user_done = false;
  states.other_user_reseted = false;
}

//---------------------------------------------------------------------------------
// checkTargets() checks if all targets are reseted
//
void checkTargets() {
  
  if(states.synched) {
    if((stop_button_state > 100 || !states.other_target_ready) && start_button_state > 100) { //target is not reseted
      displayPrompt();
      displayResetStopButtonSecondRow();
      states.ready_state = false;
      delay(50);
    } else if(stop_button_state < 100 && states.other_target_ready) {
      if(!states.ready_state) {
        displayPrompt();
        displayPressStart();
        states.ready_state = true;
      }
    }
  } else {
    if(stop_button_state > 100 && start_button_state > 100) { //target is not reseted
      displayPrompt();
      displayResetStopButtonSecondRow();
      states.ready_state = false;
      delay(50);
    } else if(stop_button_state < 100) {
      if(!states.ready_state) {
        displayPrompt();
        displayPressStart();
        states.ready_state = true;
      }
    }
  }
  
}

//----------------------------------------------------------------------------------
// loop() the main function
//
void loop() {
  
  tryToSendSynchMessage();
  sendTargetMessage();

  start_button_state = analogRead(START_BUTTON);
  stop_button_state = analogRead(STOP_BUTTON);
  
  checkTargets();

  //BUTTON is HIGH
  if((start_button_state > 100 || states.started) && states.ready_state && stop_button_state < 100) {
    //send synch to second device...
    sendMessage(STARTED, 4);
    Serial.println("I sent STARTED message");
    
    startCountDown(COUNT_DOWN_VALUE);
    switchLights(GREEN_LIGHT, RED_LIGHT);
    startTimer();
    switchLights(RED_LIGHT, GREEN_LIGHT);
    resetFunction();
    resetStates();
    Timer1.detachInterrupt();
    digitalWrite(WINNER_LAMP, LOW);
    displayPrompt();
    displayPressStart();
    delay(500);
  }

}

